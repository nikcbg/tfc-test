resource "random_pet" "example" {
  length    = "5"
}

output "name" {
  value = "${random_pet.example.id}"
}

resource "random_pet" "example1" {
  length    = "6"
}

output "name1" {
  value = "${random_pet.example1.id}"
}

resource "random_pet" "example2" {
  length    = "7"
}

output "name2" {
  value = "${random_pet.example2.id}"
}

resource "random_pet" "example3" {
  length    = "8"
}

output "name3" {
  value = "${random_pet.example3.id}"
}

resource "random_pet" "example4" {
  length    = "9"
}

output "name4" {
  value = "${random_pet.example4.id}"
}
